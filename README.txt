TiTTY, a tiny terminal emulator
------------------------------------------------------------------------------

(c) Copyright 2024, Daniel Neville, creamygoat@gmail.com

This file is part of TiTTY.

TiTTY is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

TiTTY is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with TiTTY.

------------------------------------------------------------------------------

TiTTY is a python3 script intended to be used like a serial terminal (but
using TCP/IP) and as a safer alternative to using socat or netcat on the
command line.

TiTTY was deigned with PygasusGate in mind. PygasusGate accepts a TCP/IP
connection in the case of the local keyboard being inappropritae because,
say, the machine running the stargate prop driver is itself a stage prop.

The script may be modified or simply imported so that its main() function
can be invoked with the required parameters.
