INSTRUCTIONS

Have Python3 installed

Edit titty.py or invoke it from your own Python script, choosing the
address family, IP address and port appropriately:

  import socket
  import titty
  titty.main(socket.AF_INET, "127.0.0.1", 61223, True)

If you don't yet have something to which TiTTY you would have connected,
you can use stty and netcat:

  stty -echo -icanon && nc -l 127.0.0.1 61223 ; stty sane

The "-echo" option for stty turns off the local echoing of characters
for the bash terminal (so that the remote host can echo the characters
instead). The "-icanon" turns off the line-at-a-time and line-editing
feature of the bash terminal.

Netcat (or just nc) is used here to listen ("-l") on 127.0.0.1
(loalhost), port 61223, an arbitrarily chosen number hopefully not
clashing with a de facto standard use of that port.

If you use Ctrl+C to interrupt this (compound) command, you may find
that you cannot see the commands you are typing. In that case press
Ctrl+U and then Ctrl+K (to fully clear the command line) and enter
"stty sane" to restore the terminal settings. If the terminal is
really messed up, perhaps because of a VT100 scrolling window being
activated or the tab stops being rearranged, you may additionally
enter

  echo -e "\x1bc"

to command the VT100 emulation to reset to its initial state.


Once TiTTY is running, the keyboard commands are:

  C to connect once
  A to connect and automatically reconnect on disconnection
  ^D to close the connection (or stop attempting to connect)
  ^W to exit the program. (^Q and ^S are reserved for XON and XOFF)
