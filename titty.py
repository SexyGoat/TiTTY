#!/usr/bin/env python3

"""TiTTY, a tiny Teletype
Version: 1.0.0.0
(c) Copyright 2024, Daniel Neville

This program is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""


import sys
import tty
import socket
import selectors


DEFAULT_REMOTE_AF = socket.AF_INET
DEFAULT_REMOTE_ADDR = '127.0.0.1'
DEFAULT_REMOTE_PORT = 61223
DEFAULT_AUTOCONNECT = True

CHR_EOT = chr(ord('D') & 31)  # ASCII EOT (0x04) = End Of Transmission
CHR_QUIT = chr(ord('W') & 31)  # Because ^Q and ^S are for XON and XOFF
CHR_CONNECT = 'C'
CHR_AUTOCONNECT = 'A'


def addr_str(family, addr, port):
  """Return a pretty string for thr given address and port."""
  if family == socket.AF_INET:
    fmt_str = "{addr}:{port}"
  elif family == socket.AF_INET6:
    fmt_str = "[{addr}]:{port}"
  else:
    fmt_str = "({family}, {addr}, {port})"
  return fmt_str.format(family=family, addr=addr, port=port)


def main(
  remote_af=None,
  remote_addr=None,
  remote_port=None,
  autoconnect=None
):
  """Begin a simple character-at-a-time glass teletype session.

  Arguments
    remote_af: Address Family (socket.INET, for example)
    remote_addr: Address of remote host
    remote_port: Port on remote host
    autoconnect: Automatically connect, reconnect if disconnected
  """

  if remote_af is None: remote_af = DEFAULT_REMOTE_AF
  if remote_addr is None: remote_addr = DEFAULT_REMOTE_ADDR
  if remote_port is None: remote_port = DEFAULT_REMOTE_PORT
  if autoconnect is None: autoconnect = True

  sel = None
  poll = None
  stdin_key = None
  want_connection = True
  is_reconnection = False
  announce_state = True
  sock = None
  sock_connected = False
  sock_key = None

  stdin_buffer = ""
  receive_buffer = bytearray()

  do_exit = False

  try:

    tty.setcbreak(sys.stdin.fileno())
    sel = selectors.DefaultSelector()
    stdin_key = sel.register(sys.stdin, selectors.EVENT_READ)

    while not do_exit:

      if autoconnect: want_connection = True

      if announce_state:
        if want_connection:
          astr = addr_str(remote_af, remote_addr, remote_port)
          reconn_str = "Connecting"
          if is_reconnection:
            reconn_str = "Auto-reconnecting"
          print(f"\x1b[36;1m{reconn_str} to {astr}\x1b[0m")
        announce_state = False

      if want_connection and not sock_connected:
        if sock is None:
          sock = socket.socket(remote_af, socket.SOCK_STREAM)
          sock.setblocking(False)
        else:
          rc = sock.connect_ex((remote_addr, remote_port))
          if not rc:
            sock_connected = True
            print(f"\x1b[32;1mConnected\x1b[0m")
            sock_key = sel.register(
                sock, selectors.EVENT_READ) # | selectors.EVENT_WRITE)

      events = sel.select(timeout=1.5)
      for key, mask in events:
        if key == stdin_key:
          stdin_buffer += key.fileobj.read(1)
        elif key == sock_key:
          if mask & selectors.EVENT_READ:
            data = sock.recv(1024)
            if len(data):
              receive_buffer += data
            else:
              sock_connected = False
              is_reconnection = False
              if autoconnect:
                is_reconnection = True
                announce_state = True

      while len(stdin_buffer):
        send_len = 0
        send_len = min(1024, len(stdin_buffer))
        eot_ix = stdin_buffer[:send_len].find(CHR_EOT)
        quit_ix = stdin_buffer[:send_len].find(CHR_QUIT)
        for ix in (eot_ix, quit_ix):
          if 0 <= ix < send_len: send_len = ix
        if send_len and sock_connected:
          sock.sendall(stdin_buffer[:send_len].encode(
              'ascii', errors='replace'))
          stdin_buffer = stdin_buffer[send_len:]
        if len(stdin_buffer):
          ch = stdin_buffer[0]
          uch = ch.upper()
          if ch == CHR_EOT:
            if want_connection and not sock_connected:
              print("\x1b[0;31;1mAborted\x1b[0m")
            want_connection = False
            autoconnect = False
            is_reconnection = False
          elif ch == CHR_QUIT:
            want_connection = False
            autoconnect = False
            do_exit = True
          elif uch == CHR_CONNECT and not want_connection:
            want_connection = True
            announce_state = True
          elif uch == CHR_AUTOCONNECT and not want_connection:
            autoconnect = True
            want_connection = True
            announce_state = True
          stdin_buffer = stdin_buffer[1:]

      if len(receive_buffer):
        if not do_exit:
          print(
            receive_buffer.decode('ascii', errors='replace'),
            end='', flush=True
          )
        receive_buffer = b''

      if not want_connection:
        sock_connected = False

      if not sock_connected and sock_key is not None:
        print("\x1b[0;31;1mDisconnected\x1b[0m")
        sel.unregister(sock)
        sock_key = None
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        sock = None

  finally:

    if sel is not None:
      try:
        if stdin_key is not None:
          try:
            sel.unregister(sys.stdin)
          finally:
            stdin_key = None
        if sock is not None:
          try:
            if sock_key is not None:
              try:
                sel.unregister(sock)
              finally:
                sock_key = None
          finally:
            sock.close()
            sock = None
      finally:
        sel.close()


if __name__ == '__main__':
  main()
